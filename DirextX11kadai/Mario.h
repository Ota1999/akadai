#pragma once
#include "Engine/GameObject.h"

//インクルード char
/*
#include "CharObject.h"
#include "Stage.h"
*/

class Mario : public GameObject
{
private:
	//画像番号
	int hPict_;		
	//移動
	XMVECTOR v;
	//ジャンプ用関数　ジャンプしたときに色々する
	double janp; 

	//Positionの保存
	XMVECTOR PositionSave;
	double   Position_X;

	//モーション変更管理
	int motion_;	
	//キャラ変化の高さ調整
	int chara_Up_;
	//キャラ変化の高さ調整
	int chara_Height_;
	//しゃがみ時のモーションの関与を外す
	bool crouching;
	//ジャンプ時ジャンプに固定
	bool janping;
	//キャラが中央より左にいたら中央まで動く
	bool move;
	//反転したとき
	int Reverse;
	//マリオがやられたとき
	bool Dead;

	/*
	//変数 Char
	const float DASH;	//ダッシュ時に速度を何倍にするか
	//移動
	void Move() override;
	*/
public:



	//コンストラクタ
	Mario(GameObject* parent);

	//デストラクタ
	~Mario();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	//全体の場所を取得
	void GetPosition(Transform* Position_);
	//Xの位置のみを取得
	void GetPositionX(float Position_X);

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;


};