#pragma once
#include "Engine/GameObject.h"

class Block : public GameObject
{
public:
	//コンストラクタ
	Block(GameObject* parent);

	//デストラクタ
	~Block();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Position(XMVECTOR position);
};