#include "PlayScene.h"
#include "Mario.h"
#include "Enemy.h"
#include "Stage.h"

//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	pMario = Instantiate<Mario>(this);
	pStage = Instantiate<Stage>(this);

}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}
