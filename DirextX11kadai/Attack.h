#pragma once
#include "Engine/GameObject.h"

class Attack : public GameObject
{
private:
	//画像番号
	int hPict_;
	//動くモーション管理
	int move_;

public:
	//コンストラクタ
	Attack(GameObject* parent);

	//デストラクタ
	~Attack();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//　X はよこ　Y はたて
	void Shot(XMVECTOR position);
};