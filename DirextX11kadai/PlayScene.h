#pragma once
#include "Engine/GameObject.h"

class Mario;

class Stage;

//シーンを管理するクラス
class PlayScene : public GameObject
{
	Mario* pMario;

	Stage* pStage;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	Mario* GetMario() 
	{
		return pMario;
	}

	Stage* GetStage()
	{
		return pStage;
	}
};