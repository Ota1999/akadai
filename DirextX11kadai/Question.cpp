#include "Question.h"

//コンストラクタ
Question::Question(GameObject * parent)
	:GameObject(parent, "Question")
{

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Question::~Question()
{
}

//初期化
void Question::Initialize()
{
}

//更新
void Question::Update()
{
}

//描画
void Question::Draw()
{
}

//開放
void Question::Release()
{
}

//場所の取得
void Question::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}

