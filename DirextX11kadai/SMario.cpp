#include "SMario.h"
#include "Engine/global.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "DirectXMath.h"


SMario::SMario(GameObject* parent)
	:CharObject(parent, "SMario"),
	//定数
	DASH(3.0f)	//ダッシュ時に速度を何倍にするか
{
	SPEED = 0.05f;	//移動速度
	JUMP = 0.4f;	//ジャンプ力
}


SMario::~SMario()
{
}

void SMario::Initialize()
{
	//モデルデータのロード
	//画像データのロード
	hPict_ = Image::Load("Picture/Texture.png");
	assert(hPict_ >= 0);
	//					　 左　上　幅　高さ
	Image::SetRect(hPict_, 0, 112, 16, 16);



	SetPosition(XMVectorSet(INIT_POSX, INIT_POSY, 0,0));
}

void SMario::Update()
{
	//前回の位置を記憶しておく
	XMVECTOR prevPos = transform_.position_;

	//移動
	Move();

	//前回の位置と現在の位置の差分から
	//移動ベクトルを求める
	XMVECTOR vecMove = transform_.position_ - prevPos;

	//着地判定
	Landing();

	//ステージとの当たり判定
	Collision();
}

//移動
void SMario::Move()
{
	//ダッシュ
	float	speedMul = 1.0f;	//速度の倍数（通常１、ダッシュ中はDASH）
	if (Input::IsKey(DIK_Z) || Input::IsKey(DIK_LSHIFT) ||
		Input::IsPadButton(XINPUT_GAMEPAD_A))
	{
		//速度をDASH倍にする
		speedMul = DASH;
	}


	//ジャンプ	
	if (isGround_ &&	//着地中に
		(Input::IsKey(DIK_X) || Input::IsKey(DIK_SPACE) ||	//キーボード
			Input::IsPadButton(XINPUT_GAMEPAD_B)))			//コントローラー
	{
		heightSpeed_ = JUMP;
	}

	//重力落下
	transform_.position_.vecY += heightSpeed_;
	heightSpeed_ -= GRAVITY;

	//右
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.position_.vecX += SPEED * speedMul;
	}

	//左
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.position_.vecX -= SPEED * speedMul;
	}
}

void SMario::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

void SMario::Release()
{

}