#include "CharObject.h"
#include "Stage.h"

//コンストラクタ（親も名前もなし）
CharObject::CharObject(void) :
	CharObject(nullptr, "")
{
}

//コンストラクタ（名前なし）
CharObject::CharObject(GameObject * parent) :
	CharObject(parent, "")
{
}

//コンストラクタ（標準）
CharObject::CharObject(GameObject * parent, const std::string& name)
	:GameObject(parent, name),
	//定数
	RADIUS(0.5f),	//キャラの半径
	GRAVITY(0.02f),	//重力

	//変数
	heightSpeed_(0),	//縦方向の移動量
	isGround_(false)	//着地してるかどうかのフラグ
{
	//ステージにアクセスするポインタ
	//ステージを探しておく
	pStage_ = (Stage*)FindObject("Stage");
}


////初期化
//void CharObject::Initialize()
//{
//}

////更新
//void CharObject::Update()
//{
//}

//着地判定
void CharObject::Landing()
{
	//フラグリセット
	isGround_ = false;

	//床の高さ
	float  floorHeight;
	int checkX;
	int checkY;

	//下判定(着地判定)→ステージとの当たり判定関数と同じ処理
	//上の関数に書いていたものを着地判定に持ってきた
	checkX = (int)transform_.position_.vecX;
	checkY = (int)(transform_.position_.vecY - RADIUS);	//RADIUSはキャラの半径

		//着地する先が、-1の消去範囲なら消去(-1なら１を返す)
	if (pStage_->IsDeleWall(checkX, checkY))
	{
		//	KillMe();

			//ゲームオーバーシーンへ
	}

	if (pStage_->IsWall(checkX, checkY))
	{
		floorHeight = (float)checkY + 1 + 0.1f;
	}
	else
	{
		floorHeight = 0;
	}

	//着地
	if (transform_.position_.vecY < floorHeight)
	{
		//高さを床の上にする
		transform_.position_.vecY = floorHeight;

		//下へ落ちる力を0にする
		heightSpeed_ = 0;

		//着地中フラグ
		isGround_ = true;
	}
}

//ステージとの当たり判定
void CharObject::Collision()
{
	//チェックする場所（マス単位）
	int checkX;
	int checkY;

	//左判定
	checkX = (int)(transform_.position_.vecX - RADIUS);
	checkY = (int)transform_.position_.vecY;
	if (pStage_->IsWall(checkX, checkY))
	{
		transform_.position_.vecX = (float)checkX + 1 + RADIUS;
	}
	else
	{
		transform_.position_.vecX = transform_.position_.vecX;		//移動しないように、現在位置を設定
	}

	//右判定
	checkX = (int)(transform_.position_.vecX + RADIUS);
	checkY = (int)transform_.position_.vecY;
	if (pStage_->IsWall(checkX, checkY))
	{
		transform_.position_.vecX = (float)checkX - RADIUS;
	}
	else
	{
		transform_.position_.vecX = transform_.position_.vecX;
	}

	//上判定
	checkX = (int)transform_.position_.vecX;
	checkY = (int)(transform_.position_.vecY + RADIUS * 2);
	if (pStage_->IsWall(checkX, checkY))
	{
		transform_.position_.vecY = (float)checkY - RADIUS * 2;
	}


}

////描画
//void CharObject::Draw()
//{
//}

////開放
//void CharObject::Release()
//{
//}