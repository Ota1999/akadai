#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/CsvReader.h"

//コンストラクタ
Stage::Stage(IGameObject * parent) : IGameObject(parent, "Stage")
{
	//モデル番号を初期化
	for (int i = 0; i < OBJECT_MAX; i++)
	{
		hModel_[i] = -1;
	}
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//モデルのファイル名
	std::string modelName[OBJECT_MAX]{
	//ロード処理を楽にできるよう番号とファイル名前関連付け
		"Air",				//何もない
		"Ground",			//地面
		"HatenaBlock",		//はてなブロック
		"RemainBlock",		//叩いた後に残るブロック
		"Block",			//壊せるブロック
		"PipeBottomLeft",	//土管モデルの左下
		"PipeBottomRight",	//土管モデルの右下
		"PipeTopLeft",		//土管モデルの左上
		"PipeTopRight",		//土管モデルの右上
		"Empty",				//隠しブロック
		"Rock",				//叩けない(配置)ブロック
		"Bar",				//ゴールバー
		"Pole",				//ゴールポール
		"Flag",				//旗
		"Coin"				//コイン
	};

	for (int i = OBJECT_GROUND; i < OBJECT_MAX; i++)
	{
		//モデルのロード
		hModel_[i] = Model::Load("data/Model/BLOCK/" + modelName[i] + ".fbx");
		assert(hModel_[i] >= 0);
	}

	//CSVファイルからステージ情報をロード
	CsvReader csv;
	csv.Load("data/Stage1_1.csv");

	//読み込んだcsvの値を配列に入力
	int remove_y = STAGE_HEIGHT - 1;
	for (int h = 0; h < STAGE_HEIGHT; h++)
	{
		for (int w = 0; w < STAGE_WIDTH; w++)
		{
			stage_[remove_y][w] = csv.GetValue(w, h);
		}
		remove_y--;
	}


}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	for (int h = 0; h < STAGE_HEIGHT; h++)
	{
		for (int w = 0; w < STAGE_WIDTH; w++)
		{
			int type = stage_[h][w];
			if (type != OBJECT_AIR || type >= OBJECT_MAX)
			{
				//Airモデルでずれている分戻す
				int Type = type - 1;

				//平行移動行列を作成
				D3DXMATRIX mat;
				D3DXMatrixTranslation(&mat, w, h, 0);

				//描画
				Model::SetMatrix(Type, mat);
				Model::Draw(Type);
			}
		}
	}

}

//開放
void Stage::Release()
{
}