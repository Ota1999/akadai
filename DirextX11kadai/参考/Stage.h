#pragma once

#include "Engine/GameObject/GameObject.h"

#define STAGE_HEIGHT 18
#define STAGE_WIDTH 200


//ステージを管理するクラス
class Stage : public IGameObject
{
	///////////////////////　定数　/////////////////////////////
	
	enum OBJECT_TYPE
	{	
		OBJECT_AIR,			//何もない
		OBJECT_GROUND,		//地面
		OBJECT_HATENA,		//はてなブロック
		OBJECT_REMAIN,		//叩いた後に残るブロック
		OBJECT_NORMAL,		//壊せるブロック
		OBJECT_PIPE_DL,		//土管モデルの左下
		OBJECT_PIPE_DR,		//土管モデルの右下
		OBJECT_PIPE_UL,		//土管モデルの左上
		OBJECT_PIPE_UR,		//土管モデルの右上
		OBJECT_SECRET,		//隠しブロック
		OBJECT_ROCK,		//叩けない(配置)ブロック
		OBJECT_BAR,			//ゴールバー
		OBJECT_POLE,		//ゴールポール
		OBUJECT_FLAG,		//ゴールの旗
		OBJECT_COIN,		//コイン
		OBJECT_MAX
	};
	

	///////////////////////　変数　/////////////////////////////
	
	int	hModel_[OBJECT_MAX];						//モデル番号
	int stage_[STAGE_HEIGHT][STAGE_WIDTH];			//ステージ情報
	

	///////////////////////　public関数　/////////////////////////////
public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//そこが壁かどうか
	//引数：x,y	調べる位置
	//戻値：壁ならtrue、床ならfalse
	bool IsWall(int x, int y)
	{
		for (int object = OBJECT_GROUND; object < OBJECT_MAX; object++)
		{
			if (stage_[y][x] == object)
			{
				return true;
			}
		}

		return false;
	}

	//着地するブロックが、モデルを消去させる値か（−１）
	//引数：x,y調べる位置
	//戻値：消去させる値true,普通の床などfalse
	bool IsDeleWall(int x, int y)
	{
		if (stage_[y][x] == -1)
		{
			return true;
		}
		return false;
	}
};