#pragma once
#include "Engine/GameObject.h"

class Coin : public GameObject
{
public:
	//コンストラクタ
	Coin(GameObject* parent);

	//デストラクタ
	~Coin();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	
	void Position(XMVECTOR position);
};