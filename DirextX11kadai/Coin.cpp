#include "Coin.h"

//コンストラクタ
Coin::Coin(GameObject * parent)
	:GameObject(parent, "Coin")
{
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Coin::~Coin()
{
}

//初期化
void Coin::Initialize()
{
}

//更新
void Coin::Update()
{
}

//描画
void Coin::Draw()
{
}

//開放
void Coin::Release()
{
}

//場所の取得
void Coin::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}
