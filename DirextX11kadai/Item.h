#pragma once
#include "Engine/GameObject.h"

class Item : public GameObject
{

public:

	//定数
	enum OBJECT_TYPE
	{
		OBJECT_MUSH,		//キノコ
		OBJECT_MUSH_1UP,	//1UPキノコ
		OBJECT_FLOWER,		//花
		OBJECT_STAR,		//星
		OBJECT_MAX,
	};

	int	hPict_[OBJECT_MAX];	//モデル番号
	int Item_; //画像を定期的に動かす

	//コンストラクタ
	Item(GameObject* parent);

	//デストラクタ
	~Item();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};