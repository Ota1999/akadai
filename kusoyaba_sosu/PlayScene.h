#pragma once
#include "Engine/GameObject.h"

class Mario;

class Stage;

class Texture;

//シーンを管理するクラス
class PlayScene : public GameObject
{
	Mario* pMario;

	Stage* pStage;
private:
	//時間関係

	//時間の画像表記用記述
	//秒
	int _hPict;
	//秒2桁目
	int _hPict2;
	//分
	int _hPict3;
	//それぞれのタイムの文字を送るように
	int _hTime_image[10];

	std::string sec_[11];	//秒カウント用配列

	std::string min_[11];	//分カウント用配列

	int cnt1_;

	int cnt10_;

	int minCnt_;
	//フレームをカウントする変数
	int frame_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	Mario* GetMario() 
	{
		return pMario;
	}

	Stage* GetStage()
	{
		return pStage;
	}
};