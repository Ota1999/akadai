#include "Block.h"
#include "Engine/Input.h"
#include "Stage.h"
#include "Engine/Image.h"

//コンストラクタ
Block::Block(GameObject * parent)
	:GameObject(parent, "Block")
	, DX(0), slide_(false),big_(false)
{
	//モデル番号を初期化
	for (int i = 0; i < 1; i++)
	{
		hPict_ = -1;
	}
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Block::~Block()
{
}

//初期化
void Block::Initialize()
{
	hPict_ = Image::Load("Picture/Texture2.png");
	assert(hPict_ >= 0);
	
	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;

	//はてな
	Image::SetRect(hPict_, 80, 32, 16, 16);
	//たたけない配置ブロック　現状はつかいまわし
}

//更新
void Block::Update()
{
	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}
	//左 最近のマリオみたいに後ろにも動くようになる
	if (Input::IsKey(DIK_LEFT))
	{
		//DX += 0.005f;
	}
	transform_.position_.vecX -= DX;


}

//描画
void Block::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Block::Release()
{
}

//場所の取得
void Block::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}

//何かに当たった
void Block::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//弾に当たったとき
}