#include "Question.h"
#include "Engine/Input.h"
#include "Stage.h"
#include "Engine/Image.h"

//コンストラクタ
Question::Question(GameObject * parent)
	:GameObject(parent, "Question")
	,DX(0), slide_(false),itemoff(0),question_(0), hit_(false)
{
	//モデル番号を初期化
	for (int i = 0; i < 1; i++)
	{
		hPict_ = -1;
	}
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Question::~Question()
{
}

//初期化
void Question::Initialize()
{
	for (int i = 0; i < 1; i++)
	{
		hPict_ = Image::Load("Picture/Texture2.png");
		assert(hPict_ >= 0);
	}
	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;


	//はてな
	Image::SetRect(hPict_, 0, 32, 16, 16);
	//たたけない配置ブロック　現状はつかいまわし
}

//更新
void Question::Update()
{
	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}
	//左 最近のマリオみたいに後ろにも動くようになる
	if (Input::IsKey(DIK_LEFT))
	{
		//DX += 0.005f;
	}
	transform_.position_.vecX -= DX;


	//？ブロック回転用-------------------------------------
	if (itemoff == 0)
	{
		question_++;
		switch (question_)
		{
		case 0:
			break;
		case 4:
			Image::SetRect(hPict_, 0, 32, 16, 16);
			break;
		case 8:
			Image::SetRect(hPict_, 16, 32, 16, 16);
			break;
		case 12:
			Image::SetRect(hPict_, 32, 32, 16, 16);
			break;
		case 16:
			Image::SetRect(hPict_, 48, 32, 16, 16);
			break;
		case 20:
			//永遠と回す
			question_ = 3;
			break;
		}
	}
	//？ブロック回転用おわり-------------------------------------

}

//描画
void Question::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Question::Release()
{
}

//場所の取得
void Question::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}

//何かに当たった
void Question::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//弾に当たったとき
	if (pTarget->GetObjectName() == "Mario")
	{
		if (hit_ = true)
		{
			itemoff = 1;
			Image::SetRect(hPict_, 128, 32, 16, 16);
		}
	}
}