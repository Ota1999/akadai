#include "Mario.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/BoxCollider.h"
#include "Attack.h"
#include "PlayScene.h"
#include "Stage.h"
#include "Question.h"
#include "Coin.h"
#include "Block.h"
#include "Lift.h"
#include "Enemy.h"
#include "Item.h"
#include "Star.h"
#include "Goal.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Mario::Mario(GameObject * parent)
	:GameObject(parent, "Mario"), hPict_(-1),motion_(0),janp(0.0f),Reverse(0),pose_(0),
	Position_X(0),Position_Y(0),x(0), invincible_(0), invincible_loop(0),
	invincible_Time(false),
	chara_Up_(112),chara_Height_(16),crouching(false),move(false),janping(true)
	
	//定数char
	//,DASH(3.0f)	//ダッシュ時に速度を何倍にするか
{
}

//デストラクタ
Mario::~Mario()
{ 
}

//初期化
void Mario::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Picture/Texture2.png");
	assert(hPict_ >= 0);
	//どうにかしてジャギらない拡大方法を探したい
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;

	transform_.position_.vecX -= 0.5f;
	transform_.position_.vecY += 0.75f;


	//とりあえずの判定
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
	//					　 左　上　幅　高さ
	Image::SetRect(hPict_, 80 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
}

//更新
void Mario::Update()
{
	//前の場所を覚えておく
	PositionSave = transform_.position_;
	//Xの位置のみ
	Position_X = transform_.position_.vecX; 
	Position_Y = transform_.position_.vecY;

	//左右移動
	Move();
	//しゃがみ
	Down();
	//強化
	PowerUp();
	
	//重力、ジャンプ関連
	//ジャンプボタン押したかつ接していない
	if (Input::IsKeyDown(DIK_Z) && janping == false)
	{
		janp = 0.035f;
		transform_.position_.vecY -= 0.02f - janp;
		janping = true;
		//ジャンプモーション
		Image::SetRect(hPict_, 80 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
	}

	//地面の位置にいなければ一定の位置まで落下
	if (janping == true)
	{
		//重力関連
		janp -= 0.001f;
		transform_.position_.vecY += janp;
	}
	if (janping == false)
	{
		//重力をリセット
		janp = 0.00f;
	}
	//j重力の最大速度
	if (janp <= -0.035f)
	{
		janp = -0.035f;
	}
	if (transform_.position_.vecY <= -1.1f)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
	janping = true;
}

//描画
void Mario::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Mario::Release()
{
}

void Mario::GetPosition(Transform* Position_)
{
	Position_ = &transform_;
}

void Mario::GetPositionX(float Position_X)
{
	Position_X = transform_.position_.vecX;
}


//何かに当たった
void Mario::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//なにもあたってないなら
	//GOAL-----------------------------------------------------------------------------
	if (pTarget->GetObjectName() == "Goal")
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR);
	}
	//GOAL終わり↑Stage未関与↓--------------------------------------------------------
	//敵に当たったとき
	if (pTarget->GetObjectName() == "Enemy" && janping == true)
	{

		if (invincible_Time == false)
		{
			janp = 0.035f;
			transform_.position_.vecY -= 0.02f - janp;
			janping = true;
			//ジャンプモーション
			Image::SetRect(hPict_, 80 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
		}
		else
		{
			pTarget->KillMe();
		}
	}
	else if (pTarget->GetObjectName() == "Enemy" && janping == false)
	{
		if (invincible_Time == false)
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
		}
		else
		{
			pTarget->KillMe();
		}
	}
	//アイテム関連-----------------------------------------------------------------------
	//キノコ
	if (pTarget->GetObjectName() == "Item")
	{
		pTarget->KillMe();
		pose_ = 1;
		janping = true;
	}
	//星獲得
	if (pTarget->GetObjectName() == "Star")
	{
		pTarget->KillMe();
		//星の判定
		invincible_Time = true;
		janping = true;
	}
	//Stage未関与終わり↑Stage関与 マリオの挙動修正↓------------------------------------

	//リフトイドウ
	if (pTarget->GetObjectName() == "Lift")
	{
		janping = false;
		transform_.position_.vecY += 0.01f;
	}

	if (pTarget->GetObjectName() == "Stage")
	{
		janp = 0;
		janping = false;
	}

	//ブロックをたたいた
	if (pTarget->GetObjectName() == "Block" && janp > 0)
	{
		//大きかったらブロックを破壊
		if (pose_ > 0)
		{
			pTarget->KillMe();
		}
		//ポジションを戻す
		transform_.position_.vecY = Position_Y;
		janp = 0;
		janp -= 0.01f;
	}
	//ブロックに乗った
	else if (pTarget->GetObjectName() == "Block" && janp < 0)
	{
		//ポジションを戻す
		janp = 0.001;
		janping = false;
	}

	if (pTarget->GetObjectName() == "Question" && janp > 0)
	{
		//ポジションを戻す
		transform_.position_.vecY = Position_Y;
		janp = 0;
		janp -= 0.01f;
	}
	//ブロックに乗った
	else if (pTarget->GetObjectName() == "Question" && janp < 0)
	{
		//ポジションを戻す
		janp = 0.001;
		janping = false;
	}
}
/*
///////////////////////////////////////////////////////////////////////////////////
					ここから↓マリオの更新の内容個別で整え
///////////////////////////////////////////////////////////////////////////////////
*/


void Mario::Move()
{
	//キャラの移動
	//右走る
	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//Reverseをなくす
		Reverse = 0;
		//中央より左の時本体が動く　中央ならマップが動く
		if (transform_.position_.vecX < 0)
		{
			transform_.position_.vecX += 0.02f;
		}
		//ジャンプしていなかったらモーション動く
		if (janping == false)
		{
			//Reverseをなくす
			Reverse = 0;
			motion_++;
		}
	}
	//あるく
	else if (Input::IsKey(DIK_RIGHT))
	{
		//中央より左の時本体が動く　中央ならマップが動く
		if (transform_.position_.vecX < 0)
		{
			transform_.position_.vecX += 0.01f;
		}
		//ジャンプしていなかったらモーション動く
		if (janping == false)
		{
			//Reverseをなくす
			Reverse = 0;
			motion_++;
		}

	}
	//左走る
	if (Input::IsKey(DIK_LEFT) && Input::IsKey(DIK_X))
	{
		transform_.position_.vecX -= 0.02f;
		//ジャンプしていなかったらモーション動く
		if (janping == false)
		{
			//Reverseする
			Reverse = 112;
			motion_++;
		}
	}
	else if (Input::IsKey(DIK_LEFT))
	{
		transform_.position_.vecX -= 0.01f;
		//ジャンプしていなかったらモーション動く
		if (janping == false)
		{
			//Reverseする
			Reverse = 112;
			motion_++;
		}
	}


	//見た目の変動
	//もしマリオが中央より左にいたら
	if (transform_.position_.vecX < 0)
	{
		//マップがスライドしない
		((PlayScene*)GetParent())->GetStage()->Cslide(false);
		//
		for (int y = 0; y < 403; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetBlock(y)->Cslide(false);
		}
		for (int y = 0; y < 5; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetQuestion(y)->Cslide(false);
		}
		for (int y = 0; y < 13; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetCoin(y)->Cslide(false);
		}
		for (int y = 0; y < 6; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetLift(y)->Cslide(false);
		}
		for (int y = 0; y < 8; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetGoal(y)->Cslide(false);
		}

		((PlayScene*)GetParent())->GetStage()->GetItem()->Cslide(false);
		((PlayScene*)GetParent())->GetStage()->GetStar()->Cslide(false);
		((PlayScene*)GetParent())->GetStage()->GetEnemy()->Cslide(false);
	}
	//中央にいたら
	else
	{
		//マップがスライドする
		((PlayScene*)GetParent())->GetStage()->Cslide(true);
		for (int y = 0; y < 403; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetBlock(y)->Cslide(true);
		}
		for (int y = 0; y < 5; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetQuestion(y)->Cslide(true);
		}
		for (int y = 0; y < 13; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetCoin(y)->Cslide(true);
		}
		for (int y = 0; y < 6; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetLift(y)->Cslide(true);
		}
		for (int y = 0; y < 8; y++)
		{
			((PlayScene*)GetParent())->GetStage()->GetGoal(y)->Cslide(true);
		}
		((PlayScene*)GetParent())->GetStage()->GetItem()->Cslide(true);
		((PlayScene*)GetParent())->GetStage()->GetStar()->Cslide(true);
		((PlayScene*)GetParent())->GetStage()->GetEnemy()->Cslide(true);
	}

	//動きの描写
	if (crouching == false)
	{
		//--------------------------------------------------------

		//どちらも押してないかどちらも押しているときモーションを0に
			//ジャンプしていなかったらモーション動く
		if (janping == false)
		{
			if ((!Input::IsKey(DIK_RIGHT) && !Input::IsKey(DIK_LEFT)) || ((Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_LEFT))))
			{
				motion_ = 0;
			}
			switch (motion_)
			{
			case 0:
				//					　 左　          上　      幅　高さ
				Image::SetRect(hPict_, 0 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
				break;
			case 4:
				Image::SetRect(hPict_, 16 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
				break;
			case 8:
				Image::SetRect(hPict_, 32 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
				break;
			case 12:
				Image::SetRect(hPict_, 48 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
				break;
			case 16:
				Image::SetRect(hPict_, 32 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
				break;
			case 20:
				motion_ = 3;
				break;
			}
		}
		//--------------------------------------------------------
	}
}

void Mario::Down()
{
	//しゃがむためにモーションのリセットを先に行う
	//後で地面に接していないときも付け加える
	if (Input::IsKey(DIK_S) && chara_Height_ != 16)
	{
		//モーションリセット
		motion_ = 0;
		//しゃがみのモーション
		Image::SetRect(hPict_, 96 + Reverse, chara_Up_ + invincible_, 16, chara_Height_);
		//しゃがみの判定
		crouching = true;
	}
	else {
		crouching = false;
	}

	//マップについて
	//しゃがんでいないとき 今後　ジャンプしていないとき
	//とか攻撃していないとき　とか増える

}

void Mario::PowerUp()
{//キャラの強化(仮)
	if (Input::IsKey(DIK_Q) || pose_ == 1)
	{
		//強化のための高さ調整(スーパーマリオ)
		chara_Up_ = 80;
		chara_Height_ = 32;
		pose_ = 1;

	}
	if (Input::IsKey(DIK_W) || pose_ == 2)
	{
		//強化のための高さ調整(ファイアマリオ)
		chara_Up_ = 128;
		chara_Height_ = 32;
		pose_ = 2;
	}
	if (Input::IsKey(DIK_E) || pose_ == 0)
	{
		//強化のための高さ調整(ちびマリオ)
		chara_Up_ = 112;
		chara_Height_ = 16;
		pose_ = 0;
		BoxCollider* collision =
			new BoxCollider(XMVectorSet(0, 0, 0, 0),
				XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	}
	//ファイアマリオの時、Attack(ファイアボール射出)を呼び出す
	if (chara_Up_ == 128 && Input::IsKeyDown(DIK_X))
	{
		//攻撃の呼び出し
		GameObject* PlayScene = FindObject("PlayScene");
		Attack* pAttack = Instantiate<Attack>(FindObject("PlayScene"));
		//ポジションを受け取る
		//PositionSave = transform_.position_;
		//ショットにポジションを与える
		pAttack->Shot(PositionSave);

	}

	//無敵
	if (invincible_Time == true)
	{
		invincible_loop++;
		switch (invincible_loop % 5)
		{
		case 0:
			//ファイアマリオ以外
			if (pose_ == 0 || pose_ == 1)
			{
				invincible_ = 96;
			}
			//ファイアなら
			else
			{
				invincible_ = 48;
			}
			break;
		case 4:
			if (invincible_loop >= 300)
			{
				invincible_ = 0;
				invincible_Time = false;
				invincible_loop = 0;
			}
			invincible_ = 0;
			break;

		}

	}
}

