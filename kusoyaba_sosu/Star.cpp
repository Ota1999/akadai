#include "Star.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
Star::Star(GameObject * parent)
	:GameObject(parent, "Star"), Star_(0), hPict_(-1)
{
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Star::~Star()
{
}

//初期化
void Star::Initialize()
{

	hPict_ = Image::Load("Picture/Texture.png");
	assert(hPict_ >= 0);
	//画像場所
	Image::SetRect(hPict_, 96, 0, 16, 16);	//星
	transform_.position_.vecX -= 0.2f;
	transform_.position_.vecY -= 0.6f;

	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;
}

//更新
void Star::Update()
{
	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}

	transform_.position_.vecX -= DX;

	//アイテムモーション用
	Star_++;
	switch (Star_)
	{
	case 0:
		break;
	case 4:
		Image::SetRect(hPict_, 96, 0, 16, 16);	//星
		break;
	case 8:
		Image::SetRect(hPict_, 112, 0, 16, 16);	//星
		break;
	case 12:
		Image::SetRect(hPict_, 128, 0, 16, 16);	//星
		break;
	case 16:
		Image::SetRect(hPict_, 112, 0, 16, 16);	//星
		break;
	case 20:
		//永遠と回す
		Star_ = 3;
		break;
	}
	

}

//描画
void Star::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Star::Release()
{
}