#include "Goal.h"
#include "Engine/Input.h"
#include "Stage.h"
#include "Engine/Image.h"

//コンストラクタ
Goal::Goal(GameObject * parent)
	:GameObject(parent, "Goal")
	, DX(0), slide_(false)
{
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Goal::~Goal()
{
}

//初期化
void Goal::Initialize()
{
	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;

}

//更新
void Goal::Update()
{
	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}
	//左 最近のマリオみたいに後ろにも動くようになる
	if (Input::IsKey(DIK_LEFT))
	{
		//DX += 0.005f;
	}
	transform_.position_.vecX -= DX;


}

//描画
void Goal::Draw()
{
}

//開放
void Goal::Release()
{
}

//場所の取得
void Goal::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}

//何かに当たった
void Goal::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//弾に当たったとき
}