#pragma once
#include "Engine/GameObject.h"

class Enemy : public GameObject
{
private:
	//画像番号
	int hPict_;
	//動くモーション管理
	int move_;
	//弱体化
	bool Turtle;
	//甲羅中に踏まれた
	bool Kicked;

	//Positionの保存
	XMVECTOR PositionSave;
	double   Position_X;

	//蹴られたコウラのスピード
	double Speed;

	//蘇生
	bool Rev;
	int Reversal;

	// ステージが進むかどうかの判定
	bool slide_;
	//移動関数
	double DX;


public:
	//コンストラクタ
	Enemy(GameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void Cslide(bool cslide)
	{
		slide_ = cslide;
	}
};