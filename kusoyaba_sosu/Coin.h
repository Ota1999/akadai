#pragma once
#include "Engine/GameObject.h"


class Coin : public GameObject
{
private:
	//コイン
	int	hPict_;
public:

	// ステージが進むかどうかの判定
	bool slide_;
	//移動関数
	double DX;
	//コイン獲得
	bool get_;
	//コイン回転
	int coin_;

	//コンストラクタ
	Coin(GameObject* parent);

	//デストラクタ
	~Coin();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Position(XMVECTOR position);

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

	void Cslide(bool cslide)
	{
		slide_ = cslide;
	}
};