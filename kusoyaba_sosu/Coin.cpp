#include "Coin.h"
#include "Engine/Input.h"
#include "Stage.h"
#include "Engine/Image.h"

//コンストラクタ
Coin::Coin(GameObject * parent)
	:GameObject(parent, "Coin")
	, DX(0), slide_(false),hPict_(-1),get_(false)
{
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Coin::~Coin()
{
}

//初期化
void Coin::Initialize()
{
	hPict_ = Image::Load("Picture/Texture.png");
	assert(hPict_ >= 0);
	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;

	//こいん
	Image::SetRect(hPict_, 0, 32, 16, 16);
}

//更新
void Coin::Update()
{
	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}
	//左 最近のマリオみたいに後ろにも動くようになる
	if (Input::IsKey(DIK_LEFT))
	{
		//DX += 0.005f;
	}
	transform_.position_.vecX -= DX;

	//コイン回転

	//コイン回転用
	coin_++;
	if (get_ == false)
	{
		switch (coin_)
		{
		case 0:
			break;
		case 4:
			Image::SetRect(hPict_, 0, 16, 16, 16);
			break;
		case 8:
			Image::SetRect(hPict_, 16, 16, 16, 16);
			break;
		case 12:
			Image::SetRect(hPict_, 32, 16, 16, 16);
			break;
		case 16:
			Image::SetRect(hPict_, 48, 16, 16, 16);
			break;
		case 20:
			//永遠と回す
			coin_ = 3;
			break;
		}
	}
	else {
		//獲得用

		transform_.position_.vecY += 0.003;
		switch (coin_)
		{
		case 1:
			Image::SetRect(hPict_, 64, 16, 16, 16);
			break;
		case 5:
			Image::SetRect(hPict_, 80, 16, 16, 16);
			break;
		case 9:
			Image::SetRect(hPict_, 96, 16, 16, 16);
			break;
		case 13:
			Image::SetRect(hPict_, 112, 16, 16, 16);
			break;
		case 17:
			Image::SetRect(hPict_, 64, 16, 16, 16);
			break;
		case 21:
			//消える
			KillMe();
			break;
		}
	}

}

//描画
void Coin::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Coin::Release()
{
}

//場所の取得
void Coin::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}

//何かに当たった
void Coin::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//弾に当たったとき
	if (pTarget->GetObjectName() == "Mario")
	{
		get_ = true;
		//コインの回転をリセット
		coin_ = 0;
	}
}