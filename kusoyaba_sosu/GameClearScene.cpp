#include "GameClearScene.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"


//コンストラクタ
GameClearScene::GameClearScene(GameObject * parent)
	: GameObject(parent, "GameClearScene"), hPict_(-1)
{
}

//初期化
void GameClearScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Picture/Gameclear.png");
	assert(hPict_ >= 0);
}

//更新
void GameClearScene::Update()
{
}

//描画
void GameClearScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameClearScene::Release()
{
}
