
#include "PlayScene.h"
#include "Mario.h"
#include "Enemy.h"
#include "Stage.h"
#include "Engine/Texture.h"
#include "Engine/Image.h"
#include "Engine/SceneManager.h"



//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
	, _hPict(-1), _hPict2(-1), _hPict3(-1),
	//cnt1_(10), cnt10_(10), minCnt_(10)//時間カウント用
	cnt1_(10), cnt10_(2), minCnt_(9)     //タイムアウト系
	, frame_(0)
{
}

//初期化
void PlayScene::Initialize()
{
	std::string timeName[] =
	{
		"Picture/Time/Time_0.png",
		"Picture/Time/Time_9.png",
		"Picture/Time/Time_8.png",
		"Picture/Time/Time_7.png",
		"Picture/Time/Time_6.png",
		"Picture/Time/Time_5.png",
		"Picture/Time/Time_4.png",
		"Picture/Time/Time_3.png",
		"Picture/Time/Time_2.png",
		"Picture/Time/Time_1.png",
		"Picture/Time/Time_0.png",
	};


	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";

	for (int i = 0; i < 10; i++)
	{
		_hTime_image[i] = Image::Load(timeName[i]);
	}
	//画像データのロード
	_hPict = Image::Load("Picture/Time/Time_0.png");
	assert(_hPict >= 0);
	//画像データのロード
	_hPict2 = Image::Load("Picture/Time/Time_0.png");
	assert(_hPict2 >= 0);
	//画像データのロード
	_hPict3 = Image::Load("Picture/Time/Time_0.png");
	assert(_hPict3 >= 0);

	transform_.position_.vecY = 0.8f;


	//transform_.scale_.vecX = 0.1f;
	//transform_.scale_.vecY = 0.1f;

	pMario = Instantiate<Mario>(this);
	pStage = Instantiate<Stage>(this);

}

//更新
void PlayScene::Update()
{
	frame_++;
	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{
			//最初1:00から0:59にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 1;
				cnt1_ = 0;
				minCnt_++;
			}
			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 1;
					cnt1_ = 1;
					minCnt_++;
				}
				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}
				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}
			}
		}
	}
	if (cnt1_ == 10 && cnt10_ == 10 && minCnt_ == 10)
	{
		SceneManager* pSm = (SceneManager*)FindObject("SceneManager");
		pSm->ChangeScene(SCENE_ID_GAMEOVER);
	}
}

//描画
void PlayScene::Draw()
{
	//Draw(715, 20, min_[minCnt_] + sec_[cnt10_] + sec_[cnt1_]); //
	transform_.position_.vecX = -0.7f;
	if (minCnt_ == 10)
	{
		Image::SetTransform(_hPict3, transform_ );
		Image::Draw(_hPict3);
	}
	else {
		Image::SetTransform(_hTime_image[minCnt_], transform_);
		Image::Draw(_hTime_image[minCnt_]);
	}
	transform_.position_.vecX += 0.06f;
	if (cnt10_ == 10)
	{
		Image::SetTransform(_hPict2, transform_);
		Image::Draw(_hPict2);
	}
	else {
		Image::SetTransform(_hTime_image[cnt10_],transform_);
		Image::Draw(_hTime_image[cnt10_]);
	}
	transform_.position_.vecX += 0.06f;
	if (cnt1_ == 10)
	{
		Image::SetTransform(_hPict, transform_);
		Image::Draw(_hPict);
	}
	else {
		Image::SetTransform(_hTime_image[cnt1_], transform_ );
		Image::Draw(_hTime_image[cnt1_]);
	}

}

//開放
void PlayScene::Release()
{
}