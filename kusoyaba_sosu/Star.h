#pragma once
#include "Engine/GameObject.h"

class Star : public GameObject
{

public:

	int	hPict_;	//モデル番号
	int Star_; //画像を定期的に動かす

	// ステージが進むかどうかの判定
	bool slide_;
	//移動関数
	double DX;

	//コンストラクタ
	Star(GameObject* parent);

	//デストラクタ
	~Star();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	void Cslide(bool cslide)
	{
		slide_ = cslide;
	}
};