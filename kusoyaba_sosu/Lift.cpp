#include "Lift.h"
#include "Engine/Input.h"
#include "Stage.h"
#include "Engine/Image.h"

//コンストラクタ
Lift::Lift(GameObject * parent)
	:GameObject(parent, "Lift")
	, DX(0), slide_(false), hPict_(-1)
{
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.05f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Lift::~Lift()
{
}

//初期化
void Lift::Initialize()
{
	hPict_ = Image::Load("Picture/Texture2.png");
	assert(hPict_ >= 0);

	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;

	//はてな
	Image::SetRect(hPict_, 112, 32, 16, 8);
	//たたけない配置ブロック　現状はつかいまわし
}

//更新
void Lift::Update()
{
	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}
	//左 最近のマリオみたいに後ろにも動くようになる
	if (Input::IsKey(DIK_LEFT))
	{
		//DX += 0.005f;
	}
	transform_.position_.vecX -= DX;

	transform_.position_.vecY += 0.01f;

	if (transform_.position_.vecY >= 0.8f)
	{
		transform_.position_.vecY = -0.8f;
	}

}

//描画
void Lift::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Lift::Release()
{
}

//場所の取得
void Lift::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}

//何かに当たった
void Lift::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//弾に当たったとき
}