#pragma once
#include "Engine/GameObject.h"

class Item : public GameObject
{

public:
	int	hPict_;	//モデル番号
	int Item_; //画像を定期的に動かす

	// ステージが進むかどうかの判定
	bool slide_;
	//移動関数
	double DX;

	//コンストラクタ
	Item(GameObject* parent);

	//デストラクタ
	~Item();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	void Cslide(bool cslide)
	{
		slide_ = cslide;
	}
};