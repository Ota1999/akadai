#pragma once
#include "Engine/GameObject.h"


//シーンを管理するクラス
class GameClearScene : public GameObject
{
private:
	//画像
	int hPict_;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GameClearScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};