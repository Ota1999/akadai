#include "Enemy.h"
#include "Engine/Image.h"
#include "Mario.h"
#include "Engine/Input.h"

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy"), hPict_(-1), move_(0),
	Position_X(0),Speed(0.01f),Reversal(0),
	Turtle(false),Kicked(false),Rev(false)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Picture/Texture.png");
	assert(hPict_ >= 0);
	//					　 左　上　幅　高さ
	Image::SetRect(hPict_, 0, 48, 16, 32);
	//どうにかしてジャギらない拡大方法を探したい
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;
	
	//仮で少し前に配置　後でマップで出す？
	transform_.position_.vecX += 0.5f;
	transform_.position_.vecY -= 0.6f;


	//とりあえずの判定
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//更新
void Enemy::Update()
{
	//前の場所を覚えておく
	PositionSave = transform_.position_;
	//Xの位置のみ
	Position_X = transform_.position_.vecX;


	if (Turtle == false)
	{
		transform_.position_.vecX -= Speed;
		move_++;
		switch (move_)
		{
		case 6:
			//					   左  上  幅 高さ
			Image::SetRect(hPict_,  0, 48, 16, 32);
			break;
		case 12:
			Image::SetRect(hPict_, 16, 48, 16, 32);
			break;
		case 18:
			move_ = 5;
			break;
		}
	}
	//蹴られたら進む
	if (Kicked == true)
	{
		transform_.position_.vecX += Speed * 2;
	}
	//画面外に飛んで行った場合死亡(仮)
	if (transform_.position_.vecX >= 2.0f)
	{
		KillMe();
	}


	//蘇生関連
	if (Rev == true)
	{
		Reversal++;
	}
	//現時点では1秒
	if (Reversal <= 300)
	{
		//通常に戻る
		//Turtle = false;
	}

	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}
	transform_.position_.vecX -= DX;
}

//描画
void Enemy::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Enemy::Release()
{
}
//何かに当たった
void Enemy::OnCollision(GameObject * pTarget)
{
	if (pTarget->GetObjectName() == "Mario")
	{
		//蹴られているときに踏まれた場合
		if (Kicked == true)
		{
			//止まる
			Kicked = false;
			//蘇生までカウント
			Rev = true;
		}
		//こうらの時に踏まれた場合
		else if (Turtle == true)
		{
			//一定の速度で飛んでいく
			Kicked = true;
			//蘇生カウントリセット
			Reversal = 0;
			//蘇生カウントできないように
			Rev = false;
		}
		else
		{
			//弱体化
			Image::SetRect(hPict_, 64, 48, 16, 32);
			//こうら状態になる
			Turtle = true;
			//蘇生カウント開始
			Rev = true;
		}
	}


	if (pTarget->GetObjectName() == "Block" || pTarget->GetObjectName() == "Stage")
	{
		//反転
		Speed = -Speed;
	}
}