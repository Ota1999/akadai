#include "Item.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
Item::Item(GameObject * parent)
	:GameObject(parent, "Item"),Item_(0), hPict_(0)
{
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
	//Image::SetRect(hPict_[1], 0, 16, 16, 16);	//1UPキノコ
	//Image::SetRect(hPict_[2], 0, 32, 16, 16);	//花
	//Image::SetRect(hPict_[3], 0, 96, 16, 16);	//星
}

//デストラクタ
Item::~Item()
{
}

//初期化
void Item::Initialize()
{
	hPict_ = Image::Load("Picture/Texture.png");
	assert(hPict_ >= 0);
	//画像場所
	Image::SetRect(hPict_, 0, 0, 16, 16);	//キノコ
	transform_.position_.vecX -= 0.4f;
	transform_.position_.vecY -= 0.6f;

	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;
}

//更新
void Item::Update()
{

	if (Input::IsKey(DIK_RIGHT) && Input::IsKey(DIK_X))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.02f;
		}
	}
	else if (Input::IsKey(DIK_RIGHT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX = 0.01f;
		}
	}
	else
	{
		DX = 0.00f;
	}

	transform_.position_.vecX -= DX;
}

//描画
void Item::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Item::Release()
{
}