#pragma once
#include "Engine/GameObject.h"


class Question : public GameObject
{
	//画像読み込み
	int	hPict_[2]; 
	int hit_; // 叩かれたかどうか 最初は0でマリオにたたかれたら1
	//画像の回転
	int question_;

	int stage_[191][13];			//ステージ情報

	//移動関数
	double DX;

public:
	//コンストラクタ
	Question(GameObject* parent);

	//デストラクタ
	~Question();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	
	void Position(XMVECTOR position);


	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;
};