#include "Item.h"
#include "Engine/Image.h"

//RXgN^
Item::Item(GameObject * parent)
	:GameObject(parent, "Item"),Item_(0)
{
	//ú»
	for (int i = 0; i < OBJECT_MAX; i++)
	{

		hPict_[i] = Image::Load("Picture/Texture.png");
		assert(hPict_ >= 0);
	}
	//æê
	Image::SetRect(hPict_[0], 0, 0, 16, 16);	//LmR
	Image::SetRect(hPict_[1], 0, 16, 16, 16);	//1UPLmR
	Image::SetRect(hPict_[2], 0, 32, 16, 16);	//Ô
	Image::SetRect(hPict_[3], 0, 96, 16, 16);	//¯
}

//fXgN^
Item::~Item()
{
}

//ú»
void Item::Initialize()
{
}

//XV
void Item::Update()
{
	//ACe[Vp
	Item_++;
	switch (Item_)
	{
	case 0:
		break;
	case 4:
		Image::SetRect(hPict_[2], 0, 32, 16, 16);	//Ô
		Image::SetRect(hPict_[3], 0, 96, 16, 16);	//¯
		break;
	case 8:
		Image::SetRect(hPict_[2], 0, 48, 16, 16);	//Ô
		Image::SetRect(hPict_[3], 0, 112, 16, 16);	//¯
		break;
	case 12:
		Image::SetRect(hPict_[2], 0, 64, 16, 16);	//Ô
		Image::SetRect(hPict_[3], 0, 128, 16, 16);	//¯
		break;
	case 16:
		Image::SetRect(hPict_[2], 0, 80, 16, 16);	//Ô
		Image::SetRect(hPict_[3], 0, 112, 16, 16);	//¯
		break;
	case 20:
		//iÆñ·
		Item_ = 3;
		break;
	}
}

//`æ
void Item::Draw()
{
	Image::SetTransform(hPict_[1], transform_);
	Image::Draw(hPict_[1]);
}

//Jú
void Item::Release()
{
}