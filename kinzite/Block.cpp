#include "Block.h"

//コンストラクタ
Block::Block(GameObject * parent)
	:GameObject(parent, "Block")
{
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//デストラクタ
Block::~Block()
{
}

//初期化
void Block::Initialize()
{
}

//更新
void Block::Update()
{
}

//描画
void Block::Draw()
{
}

//開放
void Block::Release()
{
}

//場所の取得
void Block::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}