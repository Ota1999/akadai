#include "Stage.h"
#include "Engine/Image.h"
#include "Engine/CsvReader.h"
#include "Engine/Input.h"
#include "PlayScene.h"
#include "Engine/BoxCollider.h"
#include "Mario.h"
#include "Enemy.h"
//ブロック関連
#include "Block.h"
#include "Question.h"
#include "Coin.h"

//コンストラクタ
Stage::Stage(GameObject * parent)
	:GameObject(parent, "Stage"), question_(0),coin_(0),
	DX(0), slide_(false)
{
	//モデル番号を初期化
	for (int i = 0; i < OBJECT_MAX; i++)
	{
		hPict_[i] = -1;
	}

	//csv呼び出し
	CsvReader csv;
	csv.Load("1-2stage.csv");

	//読み込んだcsvの値を配列に入力
	//int remove_y = STAGE_HEIGHT - 1;
	for (int x = 0; x < STAGE_WIDTH; x++)
	{
		for (int y = 0; y < STAGE_HEIGHT; y++)
		{
			//CSVの数字をstageに格納
			stage_[x][y] = csv.GetValue(x, y);

			//壊せるblock
			if (stage_[x][y] == 1)
			{
				/*
				GameObject* PlayScene = FindObject("PlayScene");
				Block* pBlock = Instantiate<Block>(FindObject("PlayScene"));
				//ポジションを受け取る
				PositionSave = transform_.position_ + (XMVectorSet(0.1* (x - 7), -0.125*(y - 6), 0, 0));
				//ショットにポジションを与える
				pBlock->Position(PositionSave);
				*/
				
			}
			//ハテナブロック
			else if (stage_[x][y] == 2)
			{
				//Instantiate<Question>(this);
				//GameObject* PlayScene = FindObject("PlayScene");
				Question* pQuestion = Instantiate<Question>(this);
				//ポジションを受け取る
				//PositionSave = transform_.position_ + (XMVectorSet(0.1* (x - 190), -0.125*(y - 12), 0, 0));
				PositionSave = transform_.position_;
				//ショットにポジションを与える
				pQuestion->Position(PositionSave);
			}
			//コイン
			else if (stage_[x][y] == 16)
			{
				
				//Instantiate<Coin>(this);
				/*
				//コイン君どうにかうごいて
				//GameObject* PlayScene = FindObject("PlayScene");
				Coin* pCoin = Instantiate<Coin>(this);
				//Coin* pCoin = Instantiate<Coin>(FindObject("PlayScene"));
				//ポジションを受け取る
				PositionSave = transform_.position_ + (XMVectorSet(0.1* (x - 190), -0.125*(y - 12), 0, 0));
				//ショットにポジションを与える
				pCoin->Position(PositionSave);
				*/
				
			}
			else if (stage_[x][y] != 0)
			{
				//背景以外
				//おもさそこそこ　課題はそれぞれ別の判定を付ける事
				//それぞれの名前を変えて、一つ一つに別々の判定を行うことができれば
				//											位置？   x               y                z  w    大きさ
				BoxCollider* collision = 
				new BoxCollider(XMVectorSet(0.1* (x - 190), -0.125*(y - 12), 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
				AddCollider(collision);
			}
		}
	}
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{

	for (int i = 0; i < OBJECT_MAX; i++)
	{

		hPict_[i] = Image::Load("Picture/Texture.png");
		assert(hPict_ >= 0);
	}
	ModelSelect();
	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;


	GameObject* PlayScene = FindObject("PlayScene");
	Enemy* pEnemy = Instantiate<Enemy>(FindObject("PlayScene"));
}

//更新
void Stage::Update()
{

	if (Input::IsKey(DIK_D) && Input::IsKey(DIK_LSHIFT))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX -= 0.02f;
		}
	}
	else if (Input::IsKey(DIK_D))
	{
		//マリオが中央にいるとき画像が進む
		if (slide_ == true)
		{
			DX -= 0.01f;
		}
	}
	//左 最近のマリオみたいに後ろにも動くようになる
	if (Input::IsKey(DIK_A))
	{
		//DX += 0.005f;
	}
	
	//？ブロック回転用おわり-------------------------------------
	//コイン回転用
	coin_++;
	switch (coin_)
	{
	case 0:
		break;
	case 4:
		Image::SetRect(hPict_[16], 0, 16, 16, 16);
		break;
	case 8:
		Image::SetRect(hPict_[16], 16, 16, 16, 16);
		break;
	case 12:
		Image::SetRect(hPict_[16], 32, 16, 16, 16);
		break;
	case 16:
		Image::SetRect(hPict_[16], 48, 16, 16, 16);
		break;
	case 20:
		Image::SetRect(hPict_[16], 64, 16, 16, 16);
		break;
	case 24:
		Image::SetRect(hPict_[16], 80, 16, 16, 16);
		break;
	case 28:
		Image::SetRect(hPict_[16], 96, 16, 16, 16);
		break;
	case 32:
		Image::SetRect(hPict_[16], 112, 16, 16, 16);
		break;
	case 36:
		//永遠と回す
		coin_ = 3;
		break;
	}
	if (hPict_[2])
	{
		int position_x = transform_.position_.vecX;
		int position_y = transform_.position_.vecY;
	}
}

//描画
void Stage::Draw()
{

	for (int x = 0; x < STAGE_WIDTH; x++)
	{
		for (int y = 0; y < STAGE_HEIGHT; y++)
		{
			//それぞれのタイプの判定
			int type = stage_[x][y];
			//背景以外を描写
			//一次的に？も
			if (type != 0 && type != 2) 
			{
				//X ÷ 10 - 0.75(初期位置) + 移動量
				transform_.position_.vecX = x - (0.90*x) - 0.75 + DX;
				//Y ÷ 8 - 0.75(初期位置) + 移動量
				// 7/8
				transform_.position_.vecY = -y + (0.875*y) + 0.75;
				//2D表記なので関係なし
				transform_.position_.vecZ = 0;

				//ずれて表記
				Image::SetTransform(hPict_[type], transform_);
				Image::Draw(hPict_[type]);
			}
		}
	}

}

//開放
void Stage::Release()
{
}

//それぞれの画像入れ
void Stage::ModelSelect()
{
	//なにもない(仮入り)      左　 上　幅　高さ
	Image::SetRect(hPict_[0], 128, 32, 16, 16);
	//こわせるblock
	Image::SetRect(hPict_[1], 80,  32, 16, 16);
	//はてな
	Image::SetRect(hPict_[2], 0,   32, 16, 16);
	//たたけない配置ブロック　現状はつかいまわし
	Image::SetRect(hPict_[3], 64,  32, 16, 16);
	//土管
	Image::SetRect(hPict_[4], 160, 16, 16, 16);
	Image::SetRect(hPict_[5], 176, 16, 16, 16);
	Image::SetRect(hPict_[6], 160, 32, 16, 16);
	Image::SetRect(hPict_[7], 176, 32, 16, 16);
	//リフト 8 記述箇所なし
	Image::SetRect(hPict_[8], 112, 32, 16,  8);
	//地面
	Image::SetRect(hPict_[9],  64, 32, 16, 16);
	//横土管
	Image::SetRect(hPict_[10],192, 16, 16, 16);
	Image::SetRect(hPict_[11],192, 32, 16, 16);
	Image::SetRect(hPict_[12],208, 16, 16, 16);
	Image::SetRect(hPict_[13],208, 32, 16, 16);
	Image::SetRect(hPict_[14],224, 16, 16, 16);
	Image::SetRect(hPict_[15],224, 32, 16, 16);
	//コイン
	Image::SetRect(hPict_[16], 0,   16, 16, 16);
	//たたいたあとのブロック つかいまわし
	Image::SetRect(hPict_[17], 64,  32, 16, 16);
}

//何かに当たった
void Stage::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Mario")
	{

	}
}