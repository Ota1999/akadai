#pragma once
#include "Engine/GameObject.h"

const int STAGE_HEIGHT = 13;//ステージの高さ
const int STAGE_WIDTH = 191;//ステージの範囲

class Stage : public GameObject
{

	//定数
	enum OBJECT_TYPE
	{
		OBJECT_AIR,			//何もない
		OBJECT_NORMAL,		//壊せるブロック
		OBJECT_HATENA,		//はてなブロック
		OBJECT_ROCK,		//叩けない(配置)ブロック
		OBJECT_PIPE_DL,		//土管モデルの左下
		OBJECT_PIPE_DR,		//土管モデルの右下
		OBJECT_PIPE_UL,		//土管モデルの左上
		OBJECT_PIPE_UR,		//土管モデルの右上
		OBJECT_LIFT,		//リフト
		OBJECT_GROUND,		//地面
		OBJECT_PIPE_DL_SIDE,//横向き土管モデルの左下
		OBJECT_PIPE_DR_SIDE,//横向き土管モデルの右下
		OBJECT_PIPE_UL_SIDE,//横向き土管モデルの左上
		OBJECT_PIPE_UR_SIDE,//横向き土管モデルの右上
		OBJECT_PIPE_UL_SIDE_SECOND,//横向き土管モデルの左上2
		OBJECT_PIPE_UR_SIDE_SECOND,//横向き土管モデルの右上2
		OBJECT_COIN,		//コイン
		OBJECT_REMAIN,		//叩いた後に残るブロック
		OBJECT_MAX
	};
private:
	int	hPict_[OBJECT_MAX];						//モデル番号
public:

	//連続して場所の移動　仮でこっちに移動
	//Transform trans;

	// ステージが進むかどうかの判定
	bool slide_;

	//移動関数
	double DX;

	//変数
	int stage_[STAGE_WIDTH][STAGE_HEIGHT];			//ステージ情報

	//Positionの保存
	XMVECTOR PositionSave;


	//？の回転確認
	int question_;
	//コイン
	int coin_;

	//コンストラクタ
	Stage(GameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void ModelSelect();

	//そこが壁かどうか
	//引数：x,y	調べる位置
	//戻値：壁ならtrue、床ならfalse
	bool IsWall(int x, int y)
	{
		for (int object = 0; object < OBJECT_MAX; object++)
		{
			if (stage_[y][x] == object)
			{
				return true;
			}
		}

		return false;
	}

	//着地するブロックが、モデルを消去させる値か（−１）
	//引数：x,y調べる位置
	//戻値：消去させる値true,普通の床などfalse
	bool IsDeleWall(int x, int y)
	{
		//if (stage_[y][x] == -1)
		//{
		//	return true;
		//}
		return false;
	}
	
	//画像がマリオが中央にいる時のみ動く用に
	void Cslide(bool cslide)
	{
		slide_ = cslide;
	}


	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;

};