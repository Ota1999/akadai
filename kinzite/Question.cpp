#include "Question.h"
#include "Engine/Image.h"
#include "Engine/CsvReader.h"
#include "Engine/Input.h"
#include "Mario.h"

//コンストラクタ
Question::Question(GameObject * parent)
	:GameObject(parent, "Question"), hit_(0)
{
	//モデル番号を初期化
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}

	//BoxCollider* collision = new BoxCollider(XMVectorSet(0.1* (x - 190), -0.125*(y - 12), 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	//AddCollider(collision);

	//csv呼び出し
	CsvReader csv;
	csv.Load("1-2stage.csv");
	for (int x = 0; x < 191; x++)
	{
		for (int y = 0; y < 13; y++)
		{
			//CSVの数字をstageに格納
			stage_[x][y] = csv.GetValue(x, y);
			//？の位置の場合
			if (stage_[x][y] == 2)
			{
				BoxCollider* collision = new BoxCollider(XMVectorSet(0.1* (x - 196), -0.125*(y - 14), 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
				AddCollider(collision);
			}
		}
	}
}

//デストラクタ
Question::~Question()
{
}

//初期化
void Question::Initialize()
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = Image::Load("Picture/Texture.png");
		assert(hPict_ >= 0);
	}
	//はてな
	Image::SetRect(hPict_[0], 0, 32, 16, 16);
	//たたけない配置ブロック　現状はつかいまわし
	Image::SetRect(hPict_[1], 64, 32, 16, 16);

	//大きさを二倍に
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;
}

//更新
void Question::Update()
{
	if (Input::IsKey(DIK_D) && Input::IsKey(DIK_LSHIFT))
	{
		//マリオが中央にいるとき画像が進む
		if (1)
		{
			DX -= 0.02f;
		}
	}
	else if (Input::IsKey(DIK_D))
	{
		//マリオが中央にいるとき画像が進む
		if (1)
		{
			DX -= 0.01f;
		}
	}

	//マリオにたたかれていない時
	if (hit_ == 0)
	{
		//？ブロック回転用-------------------------------------
		question_++;
		switch (question_)
		{
		case 0:
			break;
		case 4:
			Image::SetRect(hPict_[0], 0, 32, 16, 16);
			break;
		case 8:
			Image::SetRect(hPict_[0], 16, 32, 16, 16);
			break;
		case 12:
			Image::SetRect(hPict_[0], 32, 32, 16, 16);
			break;
		case 16:
			Image::SetRect(hPict_[0], 48, 32, 16, 16);
			break;
		case 20:
			//永遠と回す
			question_ = 3;
			break;
		}
	}
}

//描画
void Question::Draw()
{
	for (int x = 0; x < 191; x++)
	{
		for (int y = 0; y < 13; y++)
		{
			//それぞれのタイプの判定
			int type = stage_[x][y];
			//背景以外を描写
			//一次的に？も
			if (type == 2)
			{
				//X ÷ 10 - 0.75(初期位置) + 移動量
				transform_.position_.vecX = x - (0.90*x) - 0.75 + DX;
				//Y ÷ 8 - 0.75(初期位置) + 移動量
				// 7/8
				transform_.position_.vecY = -y + (0.875*y) + 0.75;
				//2D表記なので関係なし
				transform_.position_.vecZ = 0;

				//ずれて表記
				Image::SetTransform(hPict_[hit_], transform_);
				Image::Draw(hPict_[hit_]);
			}
		}
	}
}

//開放
void Question::Release()
{
}

//場所の取得
void Question::Position(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}

//何かに当たった
void Question::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//マリオが当たった時(現状は仮)
	if (pTarget->GetObjectName() == "Mario")
	{
		hit_ = 1;
	}
}

