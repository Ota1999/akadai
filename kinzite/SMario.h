#pragma once

//インクルード
#include "CharObject.h"
#include "Stage.h"

#define INIT_POSX 2
#define INIT_POSY 3.5

//プレイヤーを管理するクラス
class SMario : public CharObject
{
private:

	int hPict_;
	//変数
	const float DASH;	//ダッシュ時に速度を何倍にするか

	enum MOTION
	{
		MOTION_DEF,
		MOTION_REVERSE,

	};

	//関数

	//移動
	void Move() override;


public:
	//コンストラクタ
	SMario(GameObject* parent);

	//デストラクタ
	~SMario();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};