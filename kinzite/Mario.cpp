#include "Mario.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/BoxCollider.h"
#include "Attack.h"
#include "PlayScene.h"
#include "Stage.h"
#include "Question.h"

//コンストラクタ
Mario::Mario(GameObject * parent)
	:GameObject(parent, "Mario"), hPict_(-1),motion_(0),janp(0.0f),Reverse(0),Position_X(0),
	chara_Up_(112),chara_Height_(16),crouching(false),move(false),janping(true)
	
	//定数char
	//,DASH(3.0f)	//ダッシュ時に速度を何倍にするか
{
}

//デストラクタ
Mario::~Mario()
{ 
}

//初期化
void Mario::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Picture/Texture2.png");
	assert(hPict_ >= 0);
	//					　 左　上　幅　高さ
	Image::SetRect(hPict_, 0, 112, 16, 16);
	//どうにかしてジャギらない拡大方法を探したい
	transform_.scale_.vecX = 2.0f;
	transform_.scale_.vecY = 2.0f;

	transform_.position_.vecX -= 0.5f;
	transform_.position_.vecY += 0.75f;


	//とりあえずの判定
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//更新
void Mario::Update()
{
	//前の場所を覚えておく
	PositionSave = transform_.position_;
	//Xの位置のみ
	Position_X = transform_.position_.vecX;


	//しゃがむためにモーションのリセットを先に行う
	//後で地面に接していないときも付け加える
	if (Input::IsKey(DIK_S) && chara_Height_ != 16)
	{
		//モーションリセット
		motion_ = 0;
		//しゃがみのモーション
		Image::SetRect(hPict_, 96 + Reverse, chara_Up_, 16, chara_Height_);
		//しゃがみの判定
		crouching = true;
	}
	else {
		crouching = false;
	}

	//マップについて
	//もしマリオが中央より左にいたら
	if (transform_.position_.vecX < 0)
	{
		//マップがスライドしない
		((PlayScene*)GetParent())->GetStage()->Cslide(false);
	}
	//中央にいたら
	else
	{
		//マップがスライドする
		((PlayScene*)GetParent())->GetStage()->Cslide(true);
	}

	//しゃがんでいないとき 今後　ジャンプしていないとき
	//とか攻撃していないとき　とか増える

	if (crouching == false)
	{
		//--------------------------------------------------------
		//キャラの移動
		//右走る
		if (Input::IsKey(DIK_D) && Input::IsKey(DIK_LSHIFT))
		{
			//Reverseをなくす
			Reverse = 0;
			//中央より左の時本体が動く　中央ならマップが動く
			if (transform_.position_.vecX < 0)
			{
				transform_.position_.vecX += 0.02f;
			}
			//ジャンプしていなかったらモーション動く
			if (janping == false)
			{
				//Reverseをなくす
				Reverse = 0;
				motion_++;
			}
		}
		//あるく
		else if (Input::IsKey(DIK_D))
		{
			//中央より左の時本体が動く　中央ならマップが動く
			if (transform_.position_.vecX < 0)
			{
				transform_.position_.vecX += 0.01f;
			}
			//ジャンプしていなかったらモーション動く
			if (janping == false)
			{
				//Reverseをなくす
				Reverse = 0;
				motion_++;
			}

		}
		//左走る
		if (Input::IsKey(DIK_A) && Input::IsKey(DIK_LSHIFT))
		{
			transform_.position_.vecX -= 0.02f;
			//ジャンプしていなかったらモーション動く
			if (janping == false)
			{
				//Reverseする
				Reverse = 112;
				motion_++;
			}
		}
		else if (Input::IsKey(DIK_A))
		{
			transform_.position_.vecX -= 0.01f;
			//ジャンプしていなかったらモーション動く
			if (janping == false)
			{
				//Reverseする
				Reverse = 112;
				motion_++;
			}
		}


		//どちらも押してないかどちらも押しているときモーションを0に
			//ジャンプしていなかったらモーション動く
		if (janping == false)
		{
			if ((!Input::IsKey(DIK_A) && !Input::IsKey(DIK_D)) || ((Input::IsKey(DIK_A) && Input::IsKey(DIK_D))))
			{
				motion_ = 0;
			}
			switch (motion_)
			{
			case 0:
				//					　 左　 上　      幅　高さ
				Image::SetRect(hPict_,  0 + Reverse, chara_Up_, 16, chara_Height_);
				break;
			case 4:
				Image::SetRect(hPict_, 16 + Reverse, chara_Up_, 16, chara_Height_);
				break;
			case 8:
				Image::SetRect(hPict_, 32 + Reverse, chara_Up_, 16, chara_Height_);
				break;
			case 12:
				Image::SetRect(hPict_, 48 + Reverse, chara_Up_, 16, chara_Height_);
				break;
			case 16:
				Image::SetRect(hPict_, 32 + Reverse, chara_Up_, 16, chara_Height_);
				break;
			case 20:
				motion_ = 3;
				break;
			}
		}

		//--------------------------------------------------------

	}


	//キャラの強化(仮)
	if (Input::IsKey(DIK_Z))
	{
		//強化のための高さ調整(スーパーマリオ)
		chara_Up_ = 80;
		chara_Height_ = 32;
	}
	if (Input::IsKey(DIK_X))
	{
		//強化のための高さ調整(ファイアマリオ)
		chara_Up_ = 128;
		chara_Height_ = 32;
	}
	if (Input::IsKey(DIK_C))
	{
		//強化のための高さ調整(ちびマリオ)
		chara_Up_ = 112;
		chara_Height_ = 16;
	}
	//ファイアマリオの時、Attack(ファイアボール射出)を呼び出す
	if (chara_Up_ == 128 && Input::IsKeyDown(DIK_F))
	{
		//攻撃の呼び出し
		GameObject* PlayScene = FindObject("PlayScene");
		Attack* pAttack = Instantiate<Attack>(FindObject("PlayScene"));
		//ポジションを受け取る
		//PositionSave = transform_.position_;
		//ショットにポジションを与える
		pAttack->Shot(PositionSave);
		
	}

	//重力、ジャンプ関連
	//ジャンプボタン押したかつ接していない
	if (Input::IsKeyDown(DIK_SPACE) /*&& /*janping == false*/)
	{
		janp = 0.035f;
		transform_.position_.vecY -= 0.02f - janp;
		janping = true;
		//ジャンプモーション
		Image::SetRect(hPict_, 80 + Reverse, chara_Up_, 16, chara_Height_);
	}

	//地面の位置にいなければ一定の位置まで落下
	if (janping == true)
	{
		//重力関連
		transform_.position_.vecY += janp;
		janp -= 0.001f;
	}
	/*元重力
		if (transform_.position_.vecY > -0.64f)
		{
			//重力関連
			transform_.position_.vecY += janp;
			janp -= 0.001f;
		}
		if (transform_.position_.vecY < -0.64f)
		{
			janping = false;
			transform_.position_.vecY = -0.64f;
		}
	*/
}

//描画
void Mario::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Mario::Release()
{
}

void Mario::GetPosition(Transform* Position_)
{
	Position_ = &transform_;
}

void Mario::GetPositionX(float Position_X)
{
	Position_X = transform_.position_.vecX;
}


//何かに当たった
void Mario::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	//弾に当たったとき
	if (pTarget->GetObjectName() == "Enemy")
	{
		janp = 0.035f;
		transform_.position_.vecY -= 0.02f - janp;
		janping = true;
		//ジャンプモーション
		Image::SetRect(hPict_, 80 + Reverse, chara_Up_, 16, chara_Height_);
	}

	if (pTarget->GetObjectName() == "Stage" && (Input::IsKey(DIK_A) || Input::IsKey(DIK_D)))
	{
		//前いた場所にXだけ戻る
		transform_.position_.vecX = Position_X;
	}
	else if (pTarget->GetObjectName() == "Stage")
	{
		//全体的に戻る
		transform_.position_ = PositionSave;
		janping = false;
	}
	if (pTarget->GetObjectName() == "Question")
	{
		//確認
		int x = 1;
	}
}