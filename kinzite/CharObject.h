#pragma once
//インクルード
#include "Engine/GameObject.h"

//クラスのプロトタイプ宣言
class Stage;

//プレイヤーを管理するクラス
class CharObject : public GameObject
{
protected:
	//定数
	float SPEED;		//移動速度
	float JUMP;			//ジャンプ力
	const float RADIUS;	//キャラの半径
	const float GRAVITY;//重力

	//変数
	Stage*	pStage_;		//ステージにアクセスするためのポインタ
	float	heightSpeed_;	//高さ方向の移動速度
	bool	isGround_;		//着地してるかフラグ




	//関数

	//移動
	virtual void Move() = 0;

	//着地判定
	void Landing();

	//ステージとの当たり判定
	virtual void Collision();


public:
	//コンストラクタ
	CharObject();
	CharObject(GameObject* parent);
	CharObject(GameObject* parent, const std::string& name);

	//初期化
	virtual void Initialize() = 0;

	//更新
	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//開放
	virtual void Release() = 0;
};