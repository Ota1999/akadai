#pragma once
#include "Engine/GameObject.h"

class Score : public GameObject
{
	int hPict_[9]; //スコアの表示

public:
	//コンストラクタ
	Score(GameObject* parent);

	//デストラクタ
	~Score();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};