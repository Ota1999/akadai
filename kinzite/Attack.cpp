#include "Attack.h"
#include "Engine/Image.h"

//コンストラクタ
Attack::Attack(GameObject * parent)
	:GameObject(parent, "Attack"), hPict_(-1), move_(0)
{
}

//デストラクタ
Attack::~Attack()
{
}

//初期化
void Attack::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Picture/Fire2.png");
	assert(hPict_ >= 0);

	//大きさの変更16*16だと少々不具合が起こる
	transform_.scale_.vecX = 0.5f;
	transform_.scale_.vecY = 0.5f;
	//					　 左　上　幅　高さ
	//どうにかしてジャギらない拡大方法を探したい

	//仮で少し前に配置　後でマップで出す
	//\transform_.position_.vecX -= 0.05f;

	//とりあえずの判定
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(0.1f, 0.1f, 0.1f, 0));
	AddCollider(collision);
}

//更新
void Attack::Update()
{
	//仮の動き　バウンドするようにしたい
	transform_.position_.vecX += 0.01f;
	move_++;

	//サンフレームおきに回転する
	if(move_ == 5)
	{
		transform_.rotate_.vecZ -= 90.0f;
		move_ = 0;
	}
	//仮で少し進んだら消す
	if (transform_.position_.vecX >= 1.0f)
	{
		KillMe();
	}
}

//描画
void Attack::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Attack::Release()
{
}

//発射
void Attack::Shot(XMVECTOR position)
{
	//位置
	transform_.position_ = position;
}