#include "Score.h"

//コンストラクタ
Score::Score(GameObject * parent)
	:GameObject(parent, "Score")
{
}

//デストラクタ
Score::~Score()
{
}

//初期化
void Score::Initialize()
{
}

//更新
void Score::Update()
{
}

//描画
void Score::Draw()
{
}

//開放
void Score::Release()
{
}